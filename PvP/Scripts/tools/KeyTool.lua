dofile(("%s/%s"):format(Game.ScriptPath, "Items.lua")) -- Part of the former survival_shapes.lua
dofile(("%s/%s"):format(Game.ScriptPath, "SurvivalObjects.lua")) -- Part of the former survival_shapes.lua

KeyTool = class()

function KeyTool:client_onEquippedUpdate(primaryState, secondaryState)
    local success, result = sm.localPlayer.getRaycast(7.5)

    if success and result.type == "body" then
        local targetShape = result:getShape()

        if isAnyOf(targetShape:getShapeUuid(), { obj_survivalobject_powercoresocket, obj_survivalobject_cardreader }) then
            if primaryState == sm.tool.interactState.start then
                local params = { targetShape = targetShape, keyId = sm.localPlayer.getActiveItem() }
                self.network:sendToServer("sv_n_use", params)
            end

            return true, false
        end
    end

    return false, false
end

function KeyTool:client_onEquip()
end

function KeyTool:client_onUnequip()
end

function KeyTool:sv_n_use(params, player)
    if params.targetShape.interactable then
        params.player = player
        sm.event.sendToInteractable(params.targetShape.interactable, "sv_e_unlock", params)
    end
end
