-- include all custom event handler functions here

local function initServer(sender, eventArgs)
    sender.sv = {}
    sender.sv.saved = sender.storage:load()

    if sender.sv.saved == nil then
        sender.sv.saved = {}
        sender.sv.saved.stats = {
            hp = 100, maxhp = 100,
            food = 100, maxfood = 100,
            water = 100, maxwater = 100,
            breath = 100, maxbreath = 100
        }
        sender.sv.saved.isConscious = true
        sender.sv.saved.hasRevivalItem = false
        sender.sv.saved.isNewPlayer = true
        sender.sv.saved.inChemical = false
        sender.sv.saved.inOil = false
        sender.storage:save(sender.sv.saved)
    end

    sender:sv_init()

    return true
end

local function refreshServer(sender, eventArgs)
    sender:sv_init()

    return true
end

local function unloadServer(sender, eventArgs)
    -- TODO: make this work
    sender.storage:save(sender.sv.saved)

    return true
end

local function initClient(sender, eventArgs)
    sender.cl = {}

    if sender.player == sm.localPlayer.getPlayer() then
        if g_survivalHud then
            g_survivalHud:open()
        end

        sender.cl.hungryEffect = sm.effect.createEffect("Mechanic - StatusHungry")
        sender.cl.thirstyEffect = sm.effect.createEffect("Mechanic - StatusThirsty")
        sender.cl.underwaterEffect = sm.effect.createEffect("Mechanic - StatusUnderwater")
    end

    sender:cl_init()

    return true
end

local function refreshClient(sender, eventArgs)
    sender:cl_init()
    sm.gui.hideGui(false)
    sm.camera.setCameraState(sm.camera.state.default)
    sm.localPlayer.setLockedControls(false)

    return true
end

local function updateClientData(sender, eventArgs)
    if sm.localPlayer.getPlayer() == sender.player then
        if sender.cl.stats == nil then
            sender.cl.stats = eventArgs.data.stats -- First time copy to avoid nil errors
        end

        if g_survivalHud then
            g_survivalHud:setSliderData("Health", eventArgs.data.stats.maxhp * 10 + 1, eventArgs.data.stats.hp * 10)
            g_survivalHud:setSliderData("Food", eventArgs.data.stats.maxfood * 10 + 1, eventArgs.data.stats.food * 10)
            g_survivalHud:setSliderData("Water", eventArgs.data.stats.maxwater * 10 + 1, eventArgs.data.stats.water * 10)
            g_survivalHud:setSliderData("Breath", eventArgs.data.stats.maxbreath * 10 + 1, eventArgs.data.stats.breath * 10)
        end

        if sender.cl.hasRevivalItem ~= eventArgs.data.hasRevivalItem then
            sender.cl.revivalChewCount = 0
        end

        if sender.player.character then
            local charParam = sender.player:isMale() and 1 or 2
            sender.cl.underwaterEffect:setParameter("char", charParam)
            sender.cl.hungryEffect:setParameter("char", charParam)
            sender.cl.thirstyEffect:setParameter("char", charParam)

            if eventArgs.data.stats.breath <= 15 and not sender.cl.underwaterEffect:isPlaying() and eventArgs.data.isConscious then
                sender.cl.underwaterEffect:start()
            elseif (eventArgs.data.stats.breath > 15 or not eventArgs.data.isConscious) and sender.cl.underwaterEffect:isPlaying() then
                sender.cl.underwaterEffect:stop()
            end

            if eventArgs.data.stats.food <= 5 and not sender.cl.hungryEffect:isPlaying() and eventArgs.data.isConscious then
                sender.cl.hungryEffect:start()
            elseif (eventArgs.data.stats.food > 5 or not eventArgs.data.isConscious) and sender.cl.hungryEffect:isPlaying() then
                sender.cl.hungryEffect:stop()
            end

            if eventArgs.data.stats.water <= 5 and not sender.cl.thirstyEffect:isPlaying() and eventArgs.data.isConscious then
                sender.cl.thirstyEffect:start()
            elseif (eventArgs.data.stats.water > 5 or not eventArgs.data.isConscious) and sender.cl.thirstyEffect:isPlaying() then
                sender.cl.thirstyEffect:stop()
            end
        end

        if eventArgs.data.stats.food <= 5 and sender.cl.stats.food > 5 then
            sm.gui.displayAlertText("#{ALERT_HUNGER}", 5)
        end

        if eventArgs.data.stats.water <= 5 and sender.cl.stats.water > 5 then
            sm.gui.displayAlertText("#{ALERT_THIRST}", 5)
        end

        if eventArgs.data.stats.hp < sender.cl.stats.hp and eventArgs.data.stats.breath == 0 then
            sm.gui.displayAlertText("#{DAMAGE_BREATH}", 1)
        elseif eventArgs.data.stats.hp < sender.cl.stats.hp and eventArgs.data.stats.food == 0 then
            sm.gui.displayAlertText("#{DAMAGE_HUNGER}", 1)
        elseif eventArgs.data.stats.hp < sender.cl.stats.hp and eventArgs.data.stats.water == 0 then
            sm.gui.displayAlertText("#{DAMAGE_THIRST}", 1)
        end

        sender.cl.stats = eventArgs.data.stats
        sender.cl.isConscious = eventArgs.data.isConscious
        sender.cl.hasRevivalItem = eventArgs.data.hasRevivalItem
        sender.cl.inChemical = eventArgs.data.inChemical
        sender.cl.inOil = eventArgs.data.inOil
        sm.localPlayer.setBlockSprinting(eventArgs.data.stats.food == 0 or eventArgs.data.stats.water == 0)
    end

    return true
end

local function updateClient(sender, eventArgs)
    if sender.player == sm.localPlayer.getPlayer() then
        sender:cl_localPlayerUpdate(eventArgs.deltaTime)
    end

    return true
end

local function clientInteracts(sender, eventArgs)
    if eventArgs.state == true then
        --sender:cl_startCutscene(camera_test)
        --sender:cl_startCutscene(camera_test_joint)
        --sender:cl_startCutscene(camera_wakeup_ground)
        --sender:cl_startCutscene(camera_approach_crash)
        --sender:cl_startCutscene(camera_wakeup_crash)
        --sender:cl_startCutscene(camera_wakeup_bed)

        if not sender.cl.isConscious then
            if sender.cl.hasRevivalItem then
                if sender.cl.revivalChewCount >= eventArgs.baguetteSteps then
                    sender.network:sendToServer("sv_n_revive")
                end

                sender.cl.revivalChewCount = sender.cl.revivalChewCount + 1
                sender.network:sendToServer("sv_onEvent", { type = "character", data = "chew" })
            else
                sender.network:sendToServer("sv_n_respawn")
            end
        end
    end

    return true
end

local function updateServer(sender, eventArgs)
    if g_survivalDev and not sender.sv.saved.isConscious and not sender.sv.saved.hasRevivalItem then
        if sm.container.canSpend(sender.player:getInventory(), obj_consumable_longsandwich, 1) then
            if sm.container.beginTransaction() then
                sm.container.spend(sender.player:getInventory(), obj_consumable_longsandwich, 1, true)
                if sm.container.endTransaction() then
                    sender.sv.saved.hasRevivalItem = true
                    sender.player:sendCharacterEvent("baguette")
                    sender.network:setClientData(sender.sv.saved)
                end
            end
        end
    end

    local character = sender.player:getCharacter()

    if character then
        sender:sv_updateTumbling()
    end

    -- If respawn failed, restore the character
    if sender.sv.respawnTimeoutTimer then
        sender.sv.respawnTimeoutTimer:tick()

        if sender.sv.respawnTimeoutTimer:done() then
            sender:sv_onSpawnCharacter()
        end
    end

    sender.sv.damageCooldown:tick()
    sender.sv.impactCooldown:tick()
    sender.sv.fireDamageCooldown:tick()
    sender.sv.poisonDamageCooldown:tick()

    -- Update breathing
    if character then
        if character:isDiving() then
            sender.sv.saved.stats.breath = math.max(sender.sv.saved.stats.breath - eventArgs.breathLostPerTick, 0)

            if sender.sv.saved.stats.breath == 0 then
                sender.sv.drownTimer:tick()

                if sender.sv.drownTimer:done() then
                    if sender.sv.saved.isConscious then
                        print("'SurvivalPlayer' is drowning!")
                        sender:sv_takeDamage(eventArgs.drownDamage, "drown")
                    end

                    sender.sv.drownTimer:start(eventArgs.drownDamageCooldown)
                end
            end
        else
            sender.sv.saved.stats.breath = sender.sv.saved.stats.maxbreath
            sender.sv.drownTimer:start(eventArgs.drownDamageCooldown)
        end

        -- Spend stamina on sprinting
        if character:isSprinting() then
            sender.sv.staminaSpend = sender.sv.staminaSpend + eventArgs.sprintStaminaCost
        end

        -- Spend stamina on carrying
        if not sender.player:getCarry():isEmpty() then
            sender.sv.staminaSpend = sender.sv.staminaSpend + eventArgs.carryStaminaCost
        end
    end

    -- Update stamina, food and water stats
    if character and sender.sv.saved.isConscious and not g_godMode then
        sender.sv.statsTimer:tick()

        if sender.sv.statsTimer:done() then
            sender.sv.statsTimer:start(eventArgs.statsTickRate)

            -- Recover health from food
            if sender.sv.saved.stats.food > eventArgs.foodRecoveryThreshold then
                local fastRecoveryFraction = 0

                -- Fast recovery when food is above fast threshold
                if sender.sv.saved.stats.food > eventArgs.fastFoodRecoveryThreshold then
                    local recoverableHp = math.min(sender.sv.saved.stats.maxhp - sender.sv.saved.stats.hp, eventArgs.fastHpRecovery)
                    local foodSpend = math.min(recoverableHp * eventArgs.fastFoodCostPerHpRecovery, math.max(sender.sv.saved.stats.food - eventArgs.fastFoodRecoveryThreshold, 0))
                    local recoveredHp = foodSpend / eventArgs.fastFoodCostPerHpRecovery
                    sender.sv.saved.stats.hp = math.min(sender.sv.saved.stats.hp + recoveredHp, sender.sv.saved.stats.maxhp)
                    sender.sv.saved.stats.food = sender.sv.saved.stats.food - foodSpend
                    fastRecoveryFraction = (recoveredHp) / eventArgs.fastHpRecovery
                end

                -- Normal recovery
                local recoverableHp = math.min(sender.sv.saved.stats.maxhp - sender.sv.saved.stats.hp, eventArgs.hpRecovery * (1 - fastRecoveryFraction))
                local foodSpend = math.min(recoverableHp * eventArgs.foodCostPerHpRecovery, math.max(sender.sv.saved.stats.food - eventArgs.foodRecoveryThreshold, 0))
                local recoveredHp = foodSpend / eventArgs.foodCostPerHpRecovery
                sender.sv.saved.stats.hp = math.min(sender.sv.saved.stats.hp + foodSpend / eventArgs.foodCostPerHpRecovery, sender.sv.saved.stats.maxhp)
                sender.sv.saved.stats.food = sender.sv.saved.stats.food - foodSpend
            end

            -- Spend water and food on stamina usage
            sender.sv.saved.stats.water = math.max(sender.sv.saved.stats.water - sender.sv.staminaSpend * eventArgs.waterCostPerStamina, 0)
            sender.sv.saved.stats.food = math.max(sender.sv.saved.stats.food - sender.sv.staminaSpend * eventArgs.foodCostPerStamina, 0)
            sender.sv.staminaSpend = 0

            -- Decrease food and water with time
            sender.sv.saved.stats.food = math.max(sender.sv.saved.stats.food - eventArgs.foodLostPerSecond, 0)
            sender.sv.saved.stats.water = math.max(sender.sv.saved.stats.water - eventArgs.waterLostPerSecond, 0)

            local fatigueDamageFromHp = false

            if sender.sv.saved.stats.food <= 0 then
                sender:sv_takeDamage(eventArgs.fatigueDamageHp, "fatigue")
                fatigueDamageFromHp = true
            end

            if sender.sv.saved.stats.water <= 0 then
                if not fatigueDamageFromHp then
                    sender:sv_takeDamage(eventArgs.fatigueDamageWater, "fatigue")
                end
            end

            sender.storage:save(sender.sv.saved)
            sender.network:setClientData(sender.sv.saved)
        end
    end

    return true
end

local function onProjectileByPlayer(sender, eventArgs)
    if type(eventArgs.attacker) == "Player" then
        sender:sv_takeDamage(eventArgs.damage, "shock")
    end

    return true
end

local function onProjectileByUnit(sender, eventArgs)
    if type(eventArgs.attacker) == "Unit" or (type(eventArgs.attacker) == "Shape" and isTrapProjectile(eventArgs.projectileName)) then
        sender:sv_takeDamage(eventArgs.damage, "shock")
    end

    return true
end

local function onProjectileWhenTumbling(sender, eventArgs)
    if sender.player.character:isTumbling() then
        ApplyKnockback(sender.player.character, eventArgs.hitVelocity:normalize(), 2000)
    end

    return true
end

local function onWaterProjectile(sender, eventArgs)
    if eventArgs.projectileName == "water"  then
        sender.network:sendToClient(sender.player, "cl_n_fillWater")
    end

    return true
end

local function melee(sender, eventArgs)
    if not sm.exists(eventArgs.attacker) then
        return
    end

    local attackingCharacter = eventArgs.attacker:getCharacter()
    local playerCharacter = sender.player:getCharacter()

    if attackingCharacter ~= nil and playerCharacter ~= nil then
        local attackDirection = (eventArgs.hitPos - attackingCharacter.worldPosition):normalize()
        local directionDiff = (attackDirection - playerCharacter:getDirection()):length()
        local directionDiffThreshold = 1.6

        if directionDiff >= directionDiffThreshold and sender.sv.blocking == true then
            print("'SurvivalPlayer' blocked melee damage")
            sm.effect.playEffect("SledgehammerHit - Default", playerCharacter.worldPosition + sm.vec3.new(0, 0, 0.5) - (attackDirection - playerCharacter:getDirection()) * 0.25)
        else
            print("'SurvivalPlayer' took melee damage")
            if type(eventArgs.attacker) == "Unit" then
                sender:sv_takeDamage(eventArgs.damage, "impact")
            else
                sender.network:sendToClients("cl_n_onEvent", { event = "impact", pos = playerCharacter:getWorldPosition(), damage = eventArgs.damage * 0.01 })
            end

            -- Melee impulse
            if eventArgs.attacker then
                ApplyKnockback(sender.player.character, attackDirection, eventArgs.power)
            end
        end
    end

    return true
end

local function explosion(sender, eventArgs)
    print("'SurvivalPlayer' took explosion damage")
    sender:sv_takeDamage(eventArgs.destructionLevel * 2, "impact")

    if sender.player.character:isTumbling() then
        local knockbackDirection = (sender.player.character.worldPosition - eventArgs.center):normalize()
        ApplyKnockback(sender.player.character, knockbackDirection, 5000)
    end

    return true
end

local function collision(sender, eventArgs)
    if not sender.sv.impactCooldown:done() then
        return true
    end

    local damageFraction, tumbleTicks = CharacterCollision(sender, eventArgs.other, eventArgs.collisionPosition, eventArgs.selfPointVelocity, eventArgs.otherPointVelocity, eventArgs.collisionNormal)
    local damage = damageFraction * sender.sv.saved.stats.maxhp

    if damage > 0 or tumbleTicks > 0 then
        sender.sv.impactCooldown:start(0.25 * 40)
    end

    if damage > 0 then
        print("'SurvivalPlayer' took collision damage")
        sender:sv_takeDamage(damage, "shock")
    end

    if tumbleTicks > 0 then
        sender:sv_startTumble(tumbleTicks)

        -- Apply a tumbling impulse based on the collision velocity
        local velocityDiff = eventArgs.selfPointVelocity - eventArgs.otherPointVelocity

        if velocityDiff:length() >= FLT_EPSILON then
            local impactSpeedDiff = math.abs(velocityDiff:dot(eventArgs.collisionNormal))
            local impactSpeedOther = math.abs(eventArgs.otherPointVelocity:dot(eventArgs.collisionNormal))
            local impactSpeed = math.min(impactSpeedDiff, impactSpeedOther)
            local impulseSpeed = math.min(impactSpeed * eventArgs.tumbleImpactImpulseFraction, eventArgs.maxTumbleImpulseSpeed)
            local impulseVector = -velocityDiff:normalize() * impulseSpeed * sender.player.character.mass
            sender.player.character:applyTumblingImpulse(impulseVector)
        end
    end

    return true
end

local function removeShapes(sender, eventArgs)
    local numParts = 0
    local numBlocks = 0
    local numJoints = 0

    for _, removedShapeType in ipairs(eventArgs.removedShapes) do
        if removedShapeType.type == "block"  then
            numBlocks = numBlocks + removedShapeType.amount
        elseif removedShapeType.type == "part"  then
            numParts = numParts + removedShapeType.amount
        elseif removedShapeType.type == "joint"  then
            numJoints = numJoints + removedShapeType.amount
        end
    end

    local staminaSpend = numParts + numJoints + math.sqrt(numBlocks)
    --sender:sv_e_staminaSpend(staminaSpend)

    return true
end

local function clientCancel(sender, eventArgs)
    if sender.useCutsceneCamera and sender.currentCutscene.canSkip then
        if sender.currentCutscene.nextCutscene then
            sender:cl_startCutscene(camera_cutscenes[sender.currentCutscene.nextCutscene])
        else
            sender.useCutsceneCamera = false
            sm.gui.hideGui(false)
            sm.camera.setCameraState(sm.camera.state.default)
            sm.localPlayer.setLockedControls(false)
            sender.progress = 0
            sender.nodeIndex = 1
        end
    end

    return true
end

function PlayerEvents(player)
    _= player.events:get("server_onCreate") + initServer
    _= player.events:get("server_onRefresh") + refreshServer
    _= player.events:get("server_onDestroy") + unloadServer
    _= player.events:get("client_onCreate") + initClient
    _= player.events:get("client_onRefresh") + refreshClient
    _= player.events:get("client_onClientDataUpdate") + updateClientData
    _= player.events:get("client_onUpdate") + updateClient
    _= player.events:get("client_onInteract") + clientInteracts
    _= player.events:get("server_onFixedUpdate") + updateServer
    _= player.events:get("server_onProjectile") + onProjectileByPlayer + onProjectileByUnit + onProjectileWhenTumbling + onWaterProjectile
    _= player.events:get("server_onMelee") + melee
    _= player.events:get("server_onExplosion") + explosion
    _= player.events:get("server_onCollision") + collision
    _= player.events:get("server_onShapeRemoved") + removeShapes
    _= player.events:get("client_onCancel") + clientCancel
    --_= player.events:get("client_onReload") -- + eventHandlerFunction
end
