local function initGame(sender, eventArgs)
    print("SurvivalGame.server_onCreate")
    sender.sv = {}
    sender.sv.saved = sender.storage:load()
    print("Saved:", sender.sv.saved)

    if sender.sv.saved == nil then
        sender.sv.saved = {}
        sender.sv.saved.data = sender.data
        printf("Seed: %.0f", sender.sv.saved.data.seed)
        sender.sv.saved.overworld = sm.world.createWorld(("%s/%s"):format(Game.ScriptPath, "worlds/Overworld.lua"), "Overworld", { dev = sender.debugMode }, sender.sv.saved.data.seed)
        sender.storage:save(sender.sv.saved)
    end

    sender.data = nil
    print(sender.sv.saved.data)

    if sender.debugMode then
        g_godMode = true
        g_survivalDev = true
    end

    sender:loadCraftingRecipes()

    g_elevatorManager = ElevatorManager()
    g_elevatorManager:sv_onCreate()

    g_respawnManager = RespawnManager()
    g_respawnManager:sv_onCreate(sender.sv.saved.overworld)

    g_beaconManager = BeaconManager()
    g_beaconManager:sv_onCreate()

    g_unitManager = UnitManager()
    g_unitManager:sv_onCreate(sender.sv.saved.overworld)

    g_questManager = QuestManager()
    g_questManager:sv_onCreate(sender)
    g_questManager:sv_activateQuest(quest_use_terminal)

    if sender.debugMode then
        g_questManager:sv_completeQuest(quest_pickup_logbook)
    end

    -- Game script managed global warehouse table
    sender.warehouses = sm.storage.load(STORAGE_CHANNEL_WAREHOUSES)
    if sender.warehouses then
        print("Loaded warehouses:")
        print(sender.warehouses)
    else
        sender.warehouses = {}
        sender:sv_e_saveWarehouses()
    end

    g_warehouses = sender.warehouses

    sender.sv.time = sm.storage.load(STORAGE_CHANNEL_TIME)

    if sender.sv.time then
        print("Loaded timeData:")
        print(sender.sv.time)
    else
        sender.sv.time = {}
        sender.sv.time.timeOfDay = 6 / 24 -- 06:00
        sender.sv.time.timeProgress = true
        sm.storage.save(STORAGE_CHANNEL_TIME, sender.sv.time)
    end

    sender:sv_updateClientData()

    sender.sv.syncTimer = Timer()
    sender.sv.syncTimer:start(0)
end

local function refreshGame(sender, eventArgs)
    g_craftingRecipes = nil
    g_refineryRecipes = nil
    sender:loadCraftingRecipes()
end

local function initClient(sender, eventArgs)
    if sender.debugMode then
        sender:registerDevCommands()
    end

    sender.cl = {}
    sender.cl.time = {}
    sender.cl.time.timeOfDay = 0.0
    sender.cl.time.timeProgress = true

    sender:loadCraftingRecipes()

    if g_respawnManager == nil then
        assert(not sm.isHost)
        g_respawnManager = RespawnManager()
    end

    g_respawnManager:cl_onCreate()

    if g_beaconManager == nil then
        assert(not sm.isHost)
        g_beaconManager = BeaconManager()
    end

    g_beaconManager:cl_onCreate()

    if g_unitManager == nil then
        assert(not sm.isHost)
        g_unitManager = UnitManager()
    end

    g_unitManager:cl_onCreate()

    if g_questManager == nil then
        assert(not sm.isHost)
        g_questManager = QuestManager()
    end

    g_questManager:cl_onCreate()

    -- Music effect
    g_survivalMusic = sm.effect.createEffect("SurvivalMusic")
    assert(g_survivalMusic)

    -- Survival HUD
    g_survivalHud = sm.gui.createSurvivalHudGui()
    assert(g_survivalHud)
end

local function updateClientData(sender, eventArgs)
    sender.cl.time = eventArgs.clientData.time
end

local function serverFixedUpdate(sender, eventArgs)
        -- Update time
        local prevTime = sender.sv.time.timeOfDay

        if sender.sv.time.timeProgress then
            sender.sv.time.timeOfDay = sender.sv.time.timeOfDay + eventArgs.deltaTime / DAYCYCLE_TIME
        end

        local newDay = sender.sv.time.timeOfDay >= 1.0

        if newDay then
            sender.sv.time.timeOfDay = math.fmod(sender.sv.time.timeOfDay, 1)
        end

        if sender.sv.time.timeOfDay >= DAYCYCLE_DAWN and prevTime < DAYCYCLE_DAWN then
            g_unitManager:sv_initNewDay()
        end

        -- Ambush
        --if not sender.debugMode then
        --	for _,ambush in ipairs(AMBUSHES) do
        --		if sender.sv.time.timeOfDay >= ambush.time and (prevTime < ambush.time or newDay) then
        --			sender:sv_ambush({ magnitude = ambush.magnitude, wave = ambush.wave })
        --		end
        --	end
        --end

        -- Client and save sync
        sender.sv.syncTimer:tick()

        if sender.sv.syncTimer:done() then
            sender.sv.syncTimer:start(eventArgs.syncInterval)
            sm.storage.save(STORAGE_CHANNEL_TIME, sender.sv.time)
            sender:sv_updateClientData()
        end

        g_elevatorManager:sv_onFixedUpdate()
        g_unitManager:sv_onFixedUpdate()
        g_questManager:sv_onFixedUpdate()
end

local function clientUpdate(sender, eventArgs)
        -- Update time
        if sender.cl.time.timeProgress then
            sender.cl.time.timeOfDay = math.fmod(sender.cl.time.timeOfDay + eventArgs.deltaTime / DAYCYCLE_TIME, 1.0)
        end

        sm.game.setTimeOfDay(sender.cl.time.timeOfDay)

        -- Update lighting values
        local index = 1

        while index < #DAYCYCLE_LIGHTING_TIMES and sender.cl.time.timeOfDay >= DAYCYCLE_LIGHTING_TIMES[index + 1] do
            index = index + 1
        end

        assert(index <= #DAYCYCLE_LIGHTING_TIMES)

        local light = 0.0

        if index < #DAYCYCLE_LIGHTING_TIMES then
            local p = (sender.cl.time.timeOfDay - DAYCYCLE_LIGHTING_TIMES[index]) / (DAYCYCLE_LIGHTING_TIMES[index + 1] - DAYCYCLE_LIGHTING_TIMES[index])
            light = sm.util.lerp(DAYCYCLE_LIGHTING_VALUES[index], DAYCYCLE_LIGHTING_VALUES[index + 1], p)
        else
            light = DAYCYCLE_LIGHTING_VALUES[index]
        end

        sm.render.setOutdoorLighting(light)
end

local function playerJoin(sender, eventArgs)
    print(eventArgs.player.name, "joined the game")

    if eventArgs.firstJoin then
        local inventory = eventArgs.player:getInventory()
        sm.container.beginTransaction()

        if sender.debugMode then
            --Hotbar
            sm.container.setItem(inventory, 0, tool_sledgehammer, 1)
            sm.container.setItem(inventory, 1, tool_spudgun, 1)
            sm.container.setItem(inventory, 7, obj_plantables_potato, 50)
            sm.container.setItem(inventory, 8, tool_lift, 1)
            sm.container.setItem(inventory, 9, tool_connect, 1)

            --Actual inventory
            sm.container.setItem(inventory, 10, tool_paint, 1)
            sm.container.setItem(inventory, 11, tool_weld, 1)
        else
            sm.container.setItem(inventory, 0, tool_sledgehammer, 1)
            sm.container.setItem(inventory, 1, tool_lift, 1)
        end

        sm.container.endTransaction()
        local spawnPoint = START_AREA_SPAWN_POINT

        if sender.debugMode then
            spawnPoint = SURVIVAL_DEV_SPAWN_POINT
        end

        if not sm.exists(sender.sv.saved.overworld) then
            sm.world.loadWorld(sender.sv.saved.overworld)
        end

        sender.sv.saved.overworld:loadCell(math.floor(spawnPoint.x/64), math.floor(spawnPoint.y/64), eventArgs.player, "sv_createNewPlayer")
    else
        local inventory = eventArgs.player:getInventory()
        local sledgehammerCount = sm.container.totalQuantity(inventory, tool_sledgehammer)

        if sledgehammerCount == 0 then
            sm.container.beginTransaction()
            sm.container.collect(inventory, tool_sledgehammer, 1)
            sm.container.endTransaction()
        elseif sledgehammerCount > 1 then
            sm.container.beginTransaction()
            sm.container.spend(inventory, tool_sledgehammer, sledgehammerCount - 1)
            sm.container.endTransaction()
        end

        local liftCount = sm.container.totalQuantity(inventory, tool_lift)

        if liftCount == 0 then
            sm.container.beginTransaction()
            sm.container.collect(inventory, tool_lift, 1)
            sm.container.endTransaction()
        elseif liftCount > 1 then
            sm.container.beginTransaction()
            sm.container.spend(inventory, tool_lift, liftCount - 1)
            sm.container.endTransaction()
        end
    end

    g_unitManager:sv_onPlayerJoined(eventArgs.player)
    g_questManager:sv_onPlayerJoined(eventArgs.player)
end

local function playerLeave(sender, eventArgs)
    print(eventArgs.player.name, "left the game")
    g_elevatorManager:sv_onPlayerLeft(eventArgs.player)
end

function GameEvents(game)
    _= game.events:get("server_onCreate") + initGame
    _= game.events:get("server_onRefresh") + refreshGame
    _= game.events:get("client_onCreate") + initClient
    _= game.events:get("client_onClientDataUpdate") + updateClientData
    _= game.events:get("server_onFixedUpdate") + serverFixedUpdate
    _= game.events:get("client_onUpdate") + clientUpdate
    _= game.events:get("server_onPlayerJoined") + playerJoin
    _= game.events:get("server_onPlayerLeft") + playerLeave
end
