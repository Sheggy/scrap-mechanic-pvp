dofile(("%s/%s"):format(Game.ScriptPath, "Lib/LoadCoreLib.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "Commands/LoadCommands.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "Items.lua")) -- Part of the former survival_shapes.lua
dofile(("%s/%s"):format(Game.ScriptPath, "SurvivalObjects.lua")) -- Part of the former survival_shapes.lua
dofile(("%s/%s"):format(Game.ScriptPath, "Harvestable.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "Constants.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "managers/ElevatorManager.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "managers/RespawnManager.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "managers/BeaconManager.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "managers/UnitManager.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "managers/QuestManager.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "util/Timer.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "Units.lua"))
dofile(("%s/%s"):format(Game.ScriptPath, "GameEvents.lua"))

SurvivalGame = class(nil)
SurvivalGame.enableLimitedInventory = true
SurvivalGame.enableRestrictions = true
SurvivalGame.enableFuelConsumption = true
SurvivalGame.enableAmmoConsumption = true
SurvivalGame.enableUpgradeCost = true
SurvivalGame.commandRegistry = nil
SurvivalGame.commandInvoker = nil
SurvivalGame.events = nil
SurvivalGame.debugMode = false

local SyncInterval = 400 -- 400 ticks | 10 seconds

local function registerEvents(self)
    self.events:add("server_onCreate", EventHandler())
    self.events:add("server_onRefresh", EventHandler())
    self.events:add("client_onCreate", EventHandler())
    self.events:add("client_onClientDataUpdate", EventHandler())
    self.events:add("server_onFixedUpdate", EventHandler())
    self.events:add("client_onUpdate", EventHandler())
    self.events:add("server_onPlayerJoined", EventHandler())
    self.events:add("server_onPlayerLeft", EventHandler())
end

function SurvivalGame:__init()
    self.commandRegistry = CommandRegistry()
    self.commandInvoker = ChatCommandInvoker(self.commandRegistry)
    self.events = Dictionary()
    self.debugMode = true
    registerEvents(self)
    GameEvents(self)
end

function SurvivalGame:server_onCreate()
    local server_onCreateEvent = self.events:get("server_onCreate")

    if server_onCreateEvent:hasSubscribers() then
        server_onCreateEvent(self)
    end
end

function SurvivalGame:server_onRefresh()
    local server_onRefreshEvent = self.events:get("server_onRefresh")

    if server_onRefreshEvent:hasSubscribers() then
        server_onRefreshEvent(self)
    end
end

local function registerCommands(self)
end

function SurvivalGame:registerDevCommands()
    self.commandRegistry:addCommand("ammo", AmmoCommand(self))
    self.commandRegistry:addCommand("spudgun", SpudgunCommand(self))
    self.commandRegistry:addCommand("gatling", GatlingCommand(self))
    self.commandRegistry:addCommand("shotgun", ShotgunCommand(self))
    self.commandRegistry:addCommand("sunshake", SunshakeCommand(self))
    self.commandRegistry:addCommand("baguette", BaguetteCommand(self))
    self.commandRegistry:addCommand("keycard", KeycardCommand(self))
    self.commandRegistry:addCommand("powercore", PowercoreCommand(self))
    self.commandRegistry:addCommand("components", ComponentsCommand(self))
    self.commandRegistry:addCommand("glowsticks", GlowsticksCommand(self))
    self.commandRegistry:addCommand("tumble", TumbleCommand(self))
    self.commandRegistry:addCommand("god", GodCommand(self))
    self.commandRegistry:addCommand("respawn", RespawnCommand(self))
    self.commandRegistry:addCommand("encrypt", EncryptCommand(self))
    self.commandRegistry:addCommand("decrypt", DecryptCommand(self))
    self.commandRegistry:addCommand("limited", LimitedCommand(self))
    self.commandRegistry:addCommand("unlimited", UnlimitedCommand(self))
    self.commandRegistry:addCommand("ambush", AmbushCommand(self))
    self.commandRegistry:addCommand("recreate", RecreateCommand(self))
    self.commandRegistry:addCommand("timeofday", TimeOfDayCommand(self))
    self.commandRegistry:addCommand("timeprogress", TimeProgressCommand(self))
    self.commandRegistry:addCommand("day", DayCommand(self))
    self.commandRegistry:addCommand("spawn", SpawnCommand(self))
    self.commandRegistry:addCommand("harvestable", HarvestableCommand(self))
    self.commandRegistry:addCommand("cleardebug", ClearDebugCommand(self))
    self.commandRegistry:addCommand("export", ExportCommand(self))
    self.commandRegistry:addCommand("import", ImportCommand(self))
    self.commandRegistry:addCommand("starterkit", StarterKitCommand(self))
    self.commandRegistry:addCommand("mechanicstartkit", MechanicStartKitCommand(self))
    self.commandRegistry:addCommand("pipekit", PipeKitCommand(self))
    self.commandRegistry:addCommand("foodkit", FoodKitCommand(self))
    self.commandRegistry:addCommand("seedkit", SeedKitCommand(self))
    self.commandRegistry:addCommand("die", DieCommand(self))
    self.commandRegistry:addCommand("sethp", SetHpCommand(self))
    self.commandRegistry:addCommand("setwater", SetWaterCommand(self))
    self.commandRegistry:addCommand("setfood", SetFoodCommand(self))
    self.commandRegistry:addCommand("aggroall", AggroAllCommand(self))
    self.commandRegistry:addCommand("goto", GoToCommand(self))
    self.commandRegistry:addCommand("raid", RaidCommand(self))
    self.commandRegistry:addCommand("stopraid", StopRaidCommand(self))
    self.commandRegistry:addCommand("disableraids", DisableRaidsCommand(self))
    self.commandRegistry:addCommand("camera", CameraCommand(self))
    self.commandRegistry:addCommand("printglobals", PrintGlobalsCommand(self))
    self.commandRegistry:addCommand("clearpathnodes", ClearPathNodesCommand(self))
    self.commandRegistry:addCommand("enablepathpotatoes", EnablePathPotatoesCommand(self))
    self.commandRegistry:addCommand("activatequest", ActivateQuestCommand(self))
    self.commandRegistry:addCommand("completequest", CompleteQuestCommand(self))
    self.commandRegistry:addCommand("heal", HealCommand(self))
    self.commandRegistry:addCommand("position", PositionCommand(self))
    self.commandRegistry:addCommand("teleport", TeleportCommand(self))
    self.commandRegistry:addCommand("clear", ClearCommand(self))
end

function SurvivalGame:cl_handleCommand(args)
    args[1] = string.gsub(args[1], "/", "")
    local chatCommand = ChatCommand(args[1], args)
    self.commandInvoker:handleCommand(sm.localPlayer.getPlayer(), chatCommand)
end

function SurvivalGame:sv_teleportCallback(args, player)
    local x, y, z = args[2], args[3], args[4]

    if x and y and z then
        local position = sm.vec3.new(x, y, z)
        local cellX, cellY = math.floor(position.x/64), math.floor(position.y/64)
        self.sv.saved.overworld:loadCell(cellX, cellY, player, "sv_recreatePlayerCharacter", { pos = position, dir = player:getCharacter():getDirection() })
    else
        self.network:sendToClient(player, "client_showMessage", "Invalid coordinates")
    end
end

function SurvivalGame:sv_clearPlayerInventoryCallback(args, player)
    if self.enableLimitedInventory then
        sm.container.beginTransaction()
        local inventory = player:getInventory()

        for slot = 0, inventory:getSize() - 1, 1 do
            inventory:setItem(slot, sm.uuid.getNil(), 0)
        end

        sm.container.endTransaction()
    end
end

function SurvivalGame:sv_healCallback(args, player)
    sm.event.sendToPlayer(player, "sv_e_debug", { hp = 100 })
    sm.event.sendToPlayer(player, "sv_e_debug", { water = 100 })
    sm.event.sendToPlayer(player, "sv_e_debug", { food = 100 })
end

function SurvivalGame:sv_tumbleCallback(args, player)
    if args[2] ~= nil then
        player.character:setTumbling(args[2])
    end

    if player.character:isTumbling() then
        self.network:sendToClients("client_showMessage", "Player is tumbling")
    else
        self.network:sendToClients("client_showMessage", "Player is not tumbling")
    end
end

function SurvivalGame:sv_setHpCallback(args, player)
    sm.event.sendToPlayer(player, "sv_e_debug", { hp = args[2] })
end

function SurvivalGame:sv_setWaterCallback(args, player)
    sm.event.sendToPlayer(player, "sv_e_debug", { water = args[2] })
end

function SurvivalGame:sv_setFoodCallback(args, player)
    sm.event.sendToPlayer(player, "sv_e_debug", { food = args[2] })
end

function SurvivalGame:sv_gotoCallback(args, player)
    local pos

    if args[2] == "here" then
        pos = player.character:getWorldPosition()
    elseif args[2] == "start" then
        pos = START_AREA_SPAWN_POINT
    elseif args[2] == "hideout" then
        pos = sm.vec3.new(32, -1248, 100)
    else
        self.network:sendToClient(player, "client_showMessage", "Unknown place")
    end

    if pos then
        local cellX, cellY = math.floor(pos.x/64), math.floor(pos.y/64)
        self.sv.saved.overworld:loadCell(cellX, cellY, player, "sv_recreatePlayerCharacter", { pos = pos, dir = player.character:getDirection() })
    end
end

function SurvivalGame:sv_respawnCallback(args, player)
    sm.event.sendToPlayer(player, "sv_e_respawn")
end

function SurvivalGame:sv_printGlobalsCallback(args, player)
    print("Globals:")

    for k,_ in pairs(_G) do
        print(k)
    end
end

function SurvivalGame:sv_activateQuestCallback(args, player)
    local uuid = args[2]

    if uuid then
        g_questManager:sv_activateQuest(uuid)
    else
        g_questManager:sv_activateAllQuests()
    end
end

function SurvivalGame:sv_completeQuestCallback(args, player)
    local uuid = args[2]

    if uuid then
        g_questManager:sv_completeQuest(uuid)
    else
        g_questManager:sv_completeAllQuests()
    end
end

function SurvivalGame:sv_raiseWorldEventCallback(args, player)
    sm.event.sendToWorld(player.character:getWorld(), "sv_e_onChatCommand", args)
end

function SurvivalGame:client_onCreate()
    local client_onCreateEvent = self.events:get("client_onCreate")

    if client_onCreateEvent:hasSubscribers() then
        client_onCreateEvent(self)
    end
end

function SurvivalGame:client_onClientDataUpdate(clientData)
    local client_onClientDataUpdateEvent = self.events:get("client_onClientDataUpdate")

    if client_onClientDataUpdateEvent:hasSubscribers() then
        client_onClientDataUpdateEvent(self, { clientData = clientData })
    end
end

function SurvivalGame:cl_n_questMsg(params)
    g_questManager:cl_handleMsg(params)
end

function SurvivalGame:loadCraftingRecipes()
    -- Preload all crafting recipes
    if not g_craftingRecipes then
        local recipePaths = {
            workbench = ("%s/%s"):format(Game.RecipePath, "workbench.json"),
            dispenser = ("%s/%s"):format(Game.RecipePath, "dispenser.json"),
            cookbot = ("%s/%s"):format(Game.RecipePath, "cookbot.json"),
            craftbot = ("%s/%s"):format(Game.RecipePath, "craftbot.json"),
            dressbot = ("%s/%s"):format(Game.RecipePath, "dressbot.json")
        }
        g_craftingRecipes = {}

        for name, path in pairs(recipePaths) do
            local json = sm.json.open(path)
            local recipes = {}
            local recipesByIndex = {}

            for idx, recipe in ipairs(json) do
                recipe.craftTime = math.ceil(recipe.craftTime * 40) -- Seconds to ticks

                for _,ingredient in ipairs(recipe.ingredientList) do
                    ingredient.itemId = sm.uuid.new(ingredient.itemId) -- Prepare uuid
                end

                recipes[recipe.itemId] = recipe
                recipesByIndex[idx] = recipe
            end

            -- NOTE(daniel): Wardrobe is using 'recipes' by uuid, crafter is using 'recipesByIndex'
            g_craftingRecipes[name] = { path = path, recipes = recipes, recipesByIndex = recipesByIndex }
        end
    end

    -- Preload refinery recipes
    if not g_refineryRecipes then
        g_refineryRecipes = sm.json.open(("%s/%s"):format(Game.RecipePath, "refinery.json"))

        for _,recipe in pairs(g_refineryRecipes) do
            recipe.itemId = sm.uuid.new(recipe.itemId) -- Prepare uuid
        end
    end
end

function SurvivalGame:server_onFixedUpdate(deltaTime)
    local server_onFixedUpdateEvent = self.events:get("server_onFixedUpdate")

    if server_onFixedUpdateEvent:hasSubscribers() then
        server_onFixedUpdateEvent(self, { deltaTime = deltaTime, syncInterval = SyncInterval })
    end
end

function SurvivalGame:sv_updateClientData()
    self.network:setClientData({ time = self.sv.time })
end

function SurvivalGame:client_onUpdate(deltaTime)
    local client_onUpdateEvent = self.events:get("client_onUpdate")

    if client_onUpdateEvent:hasSubscribers() then
        client_onUpdateEvent(self, { deltaTime = deltaTime })
    end
end

function SurvivalGame:client_showMessage(params)
    sm.gui.chatMessage(params)
end

function SurvivalGame:sv_giveItem(params)
    sm.container.beginTransaction()
    sm.container.collect(params.player:getInventory(), params.item, params.quantity, false)
    sm.container.endTransaction()
end

function SurvivalGame:sv_switchGodMode()
    g_godMode = not g_godMode
    self.network:sendToClients("client_showMessage", "GODMODE: " .. (g_godMode and "On" or "Off"))
end

function SurvivalGame:sv_enableRestrictions(state)
    sm.game.enableRestrictions(state)
    self.network:sendToClients("client_showMessage", (state and "Restricted" or "Unrestricted"))
end

function SurvivalGame:sv_setLimitedInventory(state)
    sm.game.setLimitedInventory(state)
    self.network:sendToClients("client_showMessage", (state and "Limited inventory" or "Unlimited inventory"))
end

function SurvivalGame:sv_ambush(params)
    if sm.exists(self.sv.saved.overworld) then
        sm.event.sendToWorld(self.sv.saved.overworld, "sv_ambush", params)
    end
end

function SurvivalGame:sv_recreateWorld(player)
    local character = player:getCharacter()

    if character:getWorld() == self.sv.saved.overworld then
        self.sv.saved.overworld:destroy()
        self.sv.saved.overworld = sm.world.createWorld(("%s/%s"):format(Game.ScriptPath, "worlds/Overworld.lua"), "Overworld", { dev = g_survivalDev }, self.sv.saved.data.seed)
        self.storage:save(self.sv.saved)
        local params = { pos = character:getWorldPosition(), dir = character:getDirection() }
        self.sv.saved.overworld:loadCell(math.floor(params.pos.x/64), math.floor(params.pos.y/64), player, "sv_recreatePlayerCharacter", params)
        self.network:sendToClients("client_showMessage", "Recreating world")
    else
        self.network:sendToClients("client_showMessage", "Recreate world only allowed for overworld")
    end
end

function SurvivalGame:sv_setTimeOfDay(timeOfDay)
    if timeOfDay then
        self.sv.time.timeOfDay = timeOfDay
        self.sv.syncTimer.count = self.sv.syncTimer.ticks -- Force sync
    end

    self.network:sendToClients("client_showMessage", ("Time of day set to "..self.sv.time.timeOfDay))
end

function SurvivalGame:sv_setTimeProgress(timeProgress)
    if timeProgress ~= nil then
        self.sv.time.timeProgress = timeProgress
        self.sv.syncTimer.count = self.sv.syncTimer.ticks -- Force sync
    end

    self.network:sendToClients("client_showMessage", ("Time scale set to "..(self.sv.time.timeProgress and "on" or "off ")))
end

function SurvivalGame:sv_killPlayer(params)
    params.damage = 9999
    sm.event.sendToPlayer(params.player, "sv_e_receiveDamage", params)
end

function SurvivalGame:sv_spawnUnit(params)
    sm.event.sendToWorld(params.world, "sv_e_spawnUnit", params)
end

function SurvivalGame:sv_spawnHarvestable(params)
    sm.event.sendToWorld(params.world, "sv_spawnHarvestable", params)
end

function SurvivalGame:sv_exportCreation(params)
    local obj = sm.json.parseJsonString(sm.creation.exportToString(params.body))
    sm.json.save(obj, "$SURVIVAL_DATA/LocalBlueprints/"..params.name..".blueprint")
end

function SurvivalGame:sv_importCreation(params)
    sm.creation.importFromFile(params.world, "$SURVIVAL_DATA/LocalBlueprints/"..params.name..".blueprint", params.position)
end

function SurvivalGame:server_onPlayerJoined(player, firstJoin)
    local server_onPlayerJoinedEvent = self.events:get("server_onPlayerJoined")

    if server_onPlayerJoinedEvent:hasSubscribers() then
        server_onPlayerJoinedEvent(self, { player = player, firstJoin = firstJoin })
    end
end

function SurvivalGame:server_onPlayerLeft(player)
    local server_onPlayerLeftEvent = self.events:get("server_onPlayerLeft")

    if server_onPlayerLeftEvent:hasSubscribers() then
        server_onPlayerLeftEvent(self, { player = player })
    end
end

function SurvivalGame:sv_e_saveWarehouses()
    sm.storage.save(STORAGE_CHANNEL_WAREHOUSES, self.warehouses)
    print("Saved warehouses:")
    print(self.warehouses)
end

function SurvivalGame:sv_e_requestWarehouseRestrictions(params)
    -- Send the warehouse restrictions to the world that asked
    print("SurvivalGame.sv_e_requestWarehouseRestrictions")

    -- Warehouse get
    local warehouse = nil

    if params.warehouseIndex then
        warehouse = self.warehouses[params.warehouseIndex]
    end

    if warehouse then
        sm.event.sendToWorld(params.world, "server_updateRestrictions", warehouse.restrictions)
    end
end

function SurvivalGame:sv_e_setWarehouseRestrictions(params)
    -- Set the restrictions for this warehouse and propagate the restrictions to all floors

    -- Warehouse get
    local warehouse = nil

    if params.warehouseIndex then
        warehouse = self.warehouses[params.warehouseIndex]
    end

    if warehouse then
        for _, newRestrictionSetting in pairs(params.restrictions) do
            if warehouse.restrictions[newRestrictionSetting.name] then
                warehouse.restrictions[newRestrictionSetting.name].state = newRestrictionSetting.state
            else
                warehouse.restrictions[newRestrictionSetting.name] = newRestrictionSetting
            end
        end

        self.warehouses[params.warehouseIndex] = warehouse
        self:sv_e_saveWarehouses()

        for i, world in ipairs(warehouse.worlds) do
            if sm.exists(world) then
                sm.event.sendToWorld(world, "server_updateRestrictions", warehouse.restrictions)
            end
        end
    end
end

function SurvivalGame:sv_e_createElevatorDestination(params)
    print("SurvivalGame.sv_e_createElevatorDestination")
    print(params)

    -- Warehouse get or create
    local warehouse

    if params.warehouseIndex then
        warehouse = self.warehouses[params.warehouseIndex]
    else
        assert(params.name == "ELEVATOR_ENTRANCE")
        warehouse = {}
        warehouse.world = params.portal:getWorldA()
        warehouse.worlds = {}
        warehouse.exits = params.exits
        warehouse.maxLevels = params.maxLevels
        warehouse.index = #self.warehouses + 1
        warehouse.restrictions = { erasable = { name = "erasable", state = false }, connectable = { name = "connectable", state = false } }
        self.warehouses[#self.warehouses + 1] = warehouse
        self:sv_e_saveWarehouses()
    end


    -- Level up
    local level

    if params.level then
        if params.name == "ELEVATOR_UP" then
            level = params.level + 1
        elseif params.name == "ELEVATOR_DOWN" then
            level = params.level - 1
        elseif params.name == "ELEVATOR_EXIT" then
            if #warehouse.exits > 0 then
                for _,cell in ipairs(warehouse.exits) do
                    if not sm.exists(warehouse.world) then
                        sm.world.loadWorld(warehouse.world)
                    end

                    local name = params.name.." "..cell.x..","..cell.y
                    sm.portal.addWorldPortalHook(warehouse.world, name, params.portal)
                    print("Added portal hook '"..name.."' in world "..warehouse.world.id)
                    g_elevatorManager:sv_loadBForPlayersInElevator(params.portal, warehouse.world, cell.x, cell.y)
                end
            else
                sm.log.error("No exit hint found, this elevator is going nowhere!")
            end
            return
        else
            assert(false)
        end
    else
        if params.name == "ELEVATOR_EXIT" then
            level = warehouse.maxLevels
        elseif params.name == "ELEVATOR_ENTRANCE" then
            level = 1
        end
    end

    -- Create warehouse world
    local worldData = {}
    worldData.level = level
    worldData.warehouseIndex = warehouse.index
    worldData.maxLevels = warehouse.maxLevels
    local world = sm.world.createWorld(("%s/%s"):format(Game.ScriptPath, "worlds/WarehouseWorld.lua"), "WarehouseWorld", worldData)
    print("Created WarehouseWorld "..world.id)

    -- Use the same restrictions for the new floor as the other floors
    warehouse.worlds[#warehouse.worlds+1] = world

    if warehouse.restrictions then
        sm.event.sendToWorld(world, "server_updateRestrictions", warehouse.restrictions)
    end
    -- Elevator portal hook
    local name

    if params.name == "ELEVATOR_UP" then
        name = "ELEVATOR_DOWN"
    elseif params.name == "ELEVATOR_DOWN" then
        name = "ELEVATOR_UP"
    else
        name = params.name
    end

    sm.portal.addWorldPortalHook(world, name, params.portal)
    print("Added portal hook '"..name.."' in world "..world.id)
    g_elevatorManager:sv_loadBForPlayersInElevator(params.portal, world, 0, 0)
end

function SurvivalGame:sv_e_elevatorEvent(params)
    print("SurvivalGame.sv_e_elevatorEvent")
    print(params)
    g_elevatorManager[params.fn](g_elevatorManager, params)
end

function SurvivalGame:sv_createNewPlayer(world, x, y, player)
    local params = { player = player, x = x, y = y }
    sm.event.sendToWorld(self.sv.saved.overworld, "sv_spawnNewCharacter", params)
end

function SurvivalGame:sv_recreatePlayerCharacter(world, x, y, player, params)
    local yaw = math.atan2(params.dir.y, params.dir.x) - math.pi/2
    local pitch = math.asin(params.dir.z)
    local newCharacter = sm.character.createCharacter(player, self.sv.saved.overworld, params.pos, yaw, pitch)
    player:setCharacter(newCharacter)
    print("Recreate character in new world")
    print(params)
end

function SurvivalGame:sv_e_respawn(params)
    if params.player.character and sm.exists(params.player.character) then
        g_respawnManager:sv_requestRespawnCharacter(params.player)
    else
        local spawnPoint = START_AREA_SPAWN_POINT

        if g_survivalDev then
            spawnPoint = SURVIVAL_DEV_SPAWN_POINT
        end

        if not sm.exists(self.saved.overworld) then
            sm.world.loadWorld(self.saved.overworld)
        end

        self.saved.overworld:loadCell(math.floor(spawnPoint.x/64), math.floor(spawnPoint.y/64), params.player, "sv_createNewPlayer")
    end
end

function SurvivalGame:sv_loadedRespawnCell(world, x, y, player)
    g_respawnManager:sv_respawnCharacter(player, world)
end

function SurvivalGame:sv_e_onSpawnPlayerCharacter(player)
    if player.character and sm.exists(player.character) then
        g_respawnManager:sv_onSpawnCharacter(player)
        g_beaconManager:sv_onSpawnCharacter(player)
    else
        sm.log.warning("SurvivalGame.sv_e_onSpawnPlayerCharacter for a character that doesn't exist")
    end
end

function SurvivalGame:sv_e_markBag(params)
    if sm.exists(params.world) then
        sm.event.sendToWorld(params.world, "sv_e_markBag", params)
    else
        sm.log.warning("SurvivalGame.sv_e_markBag in a world that doesn't exist")
    end
end

function SurvivalGame:sv_e_unmarkBag(params)
    if sm.exists(params.world) then
        sm.event.sendToWorld(params.world, "sv_e_unmarkBag", params)
    else
        sm.log.warning("SurvivalGame.sv_e_unmarkBag in a world that doesn't exist")
    end
end

-- Beacons
function SurvivalGame:sv_e_createBeacon(params)
    if sm.exists(params.beacon.world) then
        sm.event.sendToWorld(params.beacon.world, "sv_e_createBeacon", params)
    else
        sm.log.warning("SurvivalGame.sv_e_createBeacon in a world that doesn't exist")
    end
end

function SurvivalGame:sv_e_destroyBeacon(params)
    if sm.exists(params.beacon.world) then
        sm.event.sendToWorld(params.beacon.world, "sv_e_destroyBeacon", params)
    else
        sm.log.warning("SurvivalGame.sv_e_destroyBeacon in a world that doesn't exist")
    end
end

function SurvivalGame:sv_e_unloadBeacon(params)
    if sm.exists(params.beacon.world) then
        sm.event.sendToWorld(params.beacon.world, "sv_e_unloadBeacon", params)
    else
        sm.log.warning("SurvivalGame.sv_e_unloadBeacon in a world that doesn't exist")
    end
end
