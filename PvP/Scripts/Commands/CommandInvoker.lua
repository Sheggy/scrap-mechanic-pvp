--[[
    Handles commands and invokes the registered command executor for that command
]]
CommandInvoker = class()
CommandInvoker.registry = nil

--[[
    Creates a new CommandInvoker object
    @param registry The command registry to look up command executors
]]
function CommandInvoker:__init(registry)
    self.registry = registry
end

--[[
    Handles commands requested via the in-game chat
    @param sender The original command sender
    @param command the ChatCommand object containing the necessary information for the execution of the command
]]
function CommandInvoker:handleCommand(sender, command)
    local contains, executor = self.registry:getCommandExecutor(command:getCommandText())

    if contains then
        if not executor:execute(sender, command) then
            print(("Command '%s' failed"):format(command:getCommandText()))
        end
    else
        print(("Command '%s' is not registered"):format(command:getCommandText()))
    end
end
