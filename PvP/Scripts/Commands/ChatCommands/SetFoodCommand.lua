SetFoodCommand = class(CommandExecutor)
SetFoodCommand.gameInstance = nil

function SetFoodCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "number", "food", false }
    }
    self.description = "Set player food value"
end

function SetFoodCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_setFoodCallback", command:getArguments())

    return true
end
