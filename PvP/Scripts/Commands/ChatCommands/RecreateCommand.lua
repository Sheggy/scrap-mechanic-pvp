RecreateCommand = class(CommandExecutor)
RecreateCommand.gameInstance = nil

function RecreateCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Recreate world"
end

function RecreateCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_recreateWorld", sm.localPlayer.getPlayer())

    return true
end
