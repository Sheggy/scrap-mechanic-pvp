GodCommand = class(CommandExecutor)
GodCommand.gameInstance = nil

function GodCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Mechanic characters will take no damage"
end

function GodCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_switchGodMode")

    return true
end
