LimitedCommand = class(CommandExecutor)
LimitedCommand.gameInstance = nil

function LimitedCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Use the limited inventory"
end

function LimitedCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_setLimitedInventory", true)

    return true
end
