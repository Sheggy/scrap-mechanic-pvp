TimeProgressCommand = class(CommandExecutor)
TimeProgressCommand.gameInstance = nil

function TimeProgressCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "bool", "enabled", true }
    }
    self.description = "Enables or disables time progress"
end

function TimeProgressCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_setTimeProgress", command:getArguments()[2])

    return true
end
