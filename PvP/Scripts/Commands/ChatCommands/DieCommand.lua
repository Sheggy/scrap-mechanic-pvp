DieCommand = class(CommandExecutor)
DieCommand.gameInstance = nil

function DieCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Kill the player"
end

function DieCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_killPlayer", { player = sm.localPlayer.getPlayer() })

    return true
end
