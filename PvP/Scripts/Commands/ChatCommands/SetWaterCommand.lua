SetWaterCommand = class(CommandExecutor)
SetWaterCommand.gameInstance = nil

function SetWaterCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "number", "water", false }
    }
    self.description = "Set player water value"
end

function SetWaterCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_setWaterCallback", command:getArguments())

    return true
end
