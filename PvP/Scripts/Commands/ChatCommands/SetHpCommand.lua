SetHpCommand = class(CommandExecutor)
SetHpCommand.gameInstance = nil

function SetHpCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "number", "hp", false }
    }
    self.description = "Set player hp value"
end

function SetHpCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_setHpCallback", command:getArguments())

    return true
end
