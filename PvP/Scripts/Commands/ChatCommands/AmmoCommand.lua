AmmoCommand = class(CommandExecutor)
AmmoCommand.gameInstance = nil

function AmmoCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "int", "quantity", true }
    }
    self.description = "Give ammo (default 50)"
end

function AmmoCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = obj_plantables_potato, quantity = (command:getArguments()[2] or 50) })

    return true
end
