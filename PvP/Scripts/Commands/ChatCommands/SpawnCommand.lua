SpawnCommand = class(CommandExecutor)
SpawnCommand.gameInstance = nil

function SpawnCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "string", "unitName", true }
    }
    self.description = "Spawn a unit: 'woc', 'tapebot', 'totebot', 'haybot'"
end

function SpawnCommand:execute(sender, command)
    local rayCastValid, rayCastResult = sm.localPlayer.getRaycast(100)

    if rayCastValid then
        local spawnParams = {
            uuid = sm.uuid.new("00000000-0000-0000-0000-000000000000"),
            world = sm.localPlayer.getPlayer().character:getWorld(),
            position = rayCastResult.pointWorld,
            yaw = 0.0
        }

        if command:getArguments()[2] == "woc" then
            spawnParams.uuid = unit_woc
        elseif command:getArguments()[2] == "tapebot" or command:getArguments()[2] == "tb" then
            spawnParams.uuid = unit_tapebot
        elseif command:getArguments()[2] == "redtapebot" or command:getArguments()[2] == "rtb" then
            spawnParams.uuid = unit_tapebot_red
        elseif command:getArguments()[2] == "totebot" or command:getArguments()[2] == "green" or command:getArguments()[2] == "t" then
            spawnParams.uuid = unit_totebot_green
        elseif command:getArguments()[2] == "haybot" or command:getArguments()[2] == "h" then
            spawnParams.uuid = unit_haybot
        elseif command:getArguments()[2] == "worm" then
            spawnParams.uuid = unit_worm
        elseif command:getArguments()[2] == "farmbot" or command:getArguments()[2] == "f" then
            spawnParams.uuid = unit_farmbot
        elseif command:getArguments()[2] then
            spawnParams.uuid = sm.uuid.new(command:getArguments()[2])
        end

        self.gameInstance.network:sendToServer("sv_spawnUnit", spawnParams)
    end

    return true
end
