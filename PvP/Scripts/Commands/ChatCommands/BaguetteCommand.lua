BaguetteCommand = class(CommandExecutor)
BaguetteCommand.gameInstance = nil

function BaguetteCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Give 1 revival baguette"
end

function BaguetteCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = obj_consumable_longsandwich, quantity = 1 })

    return true
end
