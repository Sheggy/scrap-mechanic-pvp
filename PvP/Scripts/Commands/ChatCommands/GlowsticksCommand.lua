GlowsticksCommand = class(CommandExecutor)
GlowsticksCommand.gameInstance = nil

function GlowsticksCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "int", "quantity", true }
    }
    self.description = "Give <quantity> components (default 10)"
end

function GlowsticksCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = obj_consumable_glowstick, quantity = (command:getArguments()[2] or 10) })

    return true
end
