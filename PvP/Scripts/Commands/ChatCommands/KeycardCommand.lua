KeycardCommand = class(CommandExecutor)
KeycardCommand.gameInstance = nil

function KeycardCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Give 1 keycard"
end

function KeycardCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = obj_survivalobject_keycard, quantity = 1 })

    return true
end
