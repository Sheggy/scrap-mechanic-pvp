EnablePathPotatoesCommand = class(CommandExecutor)
EnablePathPotatoesCommand.gameInstance = nil

function EnablePathPotatoesCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "bool", "enable", true }
    }
    self.description = "Creates path nodes at potato hits in overworld and links to previous node"
end

function EnablePathPotatoesCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
