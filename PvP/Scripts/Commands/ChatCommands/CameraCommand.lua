CameraCommand = class(CommandExecutor)
CameraCommand.gameInstance = nil

function CameraCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Spawn a SplineCamera tool"
end

function CameraCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = sm.uuid.new("5bbe87d3-d60a-48b5-9ca9-0086c80ebf7f"), quantity = 1 })

    return true
end
