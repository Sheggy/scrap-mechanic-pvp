AmbushCommand = class(CommandExecutor)
AmbushCommand.gameInstance = nil

function AmbushCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "number", "magnitude", true },
        { "int", "wave", true }
    }
    self.description = "Starts a 'random' encounter"
end

function AmbushCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_ambush", { magnitude = command:getArguments()[2] or 1, wave = command:getArguments()[3] })

    return true
end
