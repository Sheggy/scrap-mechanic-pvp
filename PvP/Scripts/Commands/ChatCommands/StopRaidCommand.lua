StopRaidCommand = class(CommandExecutor)
StopRaidCommand.gameInstance = nil

function StopRaidCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Cancel all incoming raids"
end

function StopRaidCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
