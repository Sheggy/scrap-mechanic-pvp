TimeOfDayCommand = class(CommandExecutor)
TimeOfDayCommand.gameInstance = nil

function TimeOfDayCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "number", "timeOfDay", true }
    }
    self.description = "Sets the time of the day as a fraction (0.5=mid day)"
end

function TimeOfDayCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_setTimeOfDay", command:getArguments()[2])

    return true
end
