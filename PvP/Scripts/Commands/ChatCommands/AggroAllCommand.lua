AggroAllCommand = class(CommandExecutor)
AggroAllCommand.gameInstance = nil

function AggroAllCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "All hostile units will be made aware of the player's position"
end

function AggroAllCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
