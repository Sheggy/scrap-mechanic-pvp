CompleteQuestCommand = class(CommandExecutor)
CompleteQuestCommand.gameInstance = nil

function CompleteQuestCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "string", "uuid", true }
    }
    self.description = "Complete quest"
end

function CompleteQuestCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_completeQuestCallback", command:getArguments())

    return true
end
