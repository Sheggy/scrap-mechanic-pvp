SunshakeCommand = class(CommandExecutor)
SunshakeCommand.gameInstance = nil

function SunshakeCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Give 1 sunshake"
end

function SunshakeCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = obj_consumable_sunshake, quantity = 1 })

    return true
end
