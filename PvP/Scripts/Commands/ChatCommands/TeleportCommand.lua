TeleportCommand = class(CommandExecutor)
TeleportCommand.gameInstance = nil

function TeleportCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "number", "x", true },
        { "number", "y", true },
        { "number", "z", true }
    }
    self.description = "Teleports you to the specified coordinates"
end

function TeleportCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_teleportCallback", command:getArguments())

    return true
end
