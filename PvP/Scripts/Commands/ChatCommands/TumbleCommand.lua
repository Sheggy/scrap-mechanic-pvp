TumbleCommand = class(CommandExecutor)
TumbleCommand.gameInstance = nil

function TumbleCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "bool", "enable", true }
    }
    self.description = "Set tumble state"
end

function TumbleCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_tumbleCallback", command:getArguments())

    return true
end
