ActivateQuestCommand = class(CommandExecutor)
ActivateQuestCommand.gameInstance = nil

function ActivateQuestCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "string", "uuid", true }
    }
    self.description = "Activate quest"
end

function ActivateQuestCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_activateQuestCallback", command:getArguments())

    return true
end
