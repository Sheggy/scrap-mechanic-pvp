PositionCommand = class(CommandExecutor)
PositionCommand.gameInstance = nil

function PositionCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Displays the current player coordinates"
end

function PositionCommand:execute(sender, command)
    local position = sm.localPlayer.getPlayer():getCharacter():getWorldPosition()
    self.gameInstance:client_showMessage(("Current player position:\nX: %i, Y: %i, Z: %i"):format(position.x, position.y, position.z))

    return true
end
