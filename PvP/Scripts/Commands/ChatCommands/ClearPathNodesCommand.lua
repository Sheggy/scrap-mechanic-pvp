ClearPathNodesCommand = class(CommandExecutor)
ClearPathNodesCommand.gameInstance = nil

function ClearPathNodesCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Clear all path nodes in overworld"
end

function ClearPathNodesCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
