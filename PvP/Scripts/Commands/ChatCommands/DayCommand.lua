DayCommand = class(CommandExecutor)
DayCommand.gameInstance = nil

function DayCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Disable time progression and set time to daytime"
end

function DayCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_setTimeOfDay", 0.5)
    self.gameInstance.network:sendToServer("sv_setTimeProgress", false)

    return true
end
