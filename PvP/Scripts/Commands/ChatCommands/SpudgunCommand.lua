SpudgunCommand = class(CommandExecutor)
SpudgunCommand.gameInstance = nil

function SpudgunCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Give the spudgun"
end

function SpudgunCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = tool_spudgun, quantity = 1 })

    return true
end
