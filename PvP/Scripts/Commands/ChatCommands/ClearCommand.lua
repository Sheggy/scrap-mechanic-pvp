ClearCommand = class(CommandExecutor)
ClearCommand.gameInstance = nil

function ClearCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Clears your inventory"
end

function ClearCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_clearPlayerInventoryCallback", command:getArguments())

    return true
end
