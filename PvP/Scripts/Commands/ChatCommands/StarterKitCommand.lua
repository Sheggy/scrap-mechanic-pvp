StarterKitCommand = class(CommandExecutor)
StarterKitCommand.gameInstance = nil

function StarterKitCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Spawn a starter kit"
end

function StarterKitCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
