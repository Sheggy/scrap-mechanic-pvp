PowercoreCommand = class(CommandExecutor)
PowercoreCommand.gameInstance = nil

function PowercoreCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Give 1 powercore"
end

function PowercoreCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = obj_survivalobject_powercore, quantity = 1 })

    return true
end
