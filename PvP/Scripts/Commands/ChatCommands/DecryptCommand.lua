DecryptCommand = class(CommandExecutor)
DecryptCommand.gameInstance = nil

function DecryptCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Unrestrict interactions in all warehouses"
end

function DecryptCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_enableRestrictions", false)

    return true
end
