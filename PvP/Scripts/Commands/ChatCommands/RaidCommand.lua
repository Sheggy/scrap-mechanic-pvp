RaidCommand = class(CommandExecutor)
RaidCommand.gameInstance = nil

function RaidCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "int", "level", false },
        { "int", "wave", true },
        { "number", "hours", true }
    }
    self.description = "Start a level <level> raid at player position at wave <wave> in <delay> hours."
end

function RaidCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
