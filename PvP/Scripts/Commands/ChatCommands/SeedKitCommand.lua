SeedKitCommand = class(CommandExecutor)
SeedKitCommand.gameInstance = nil

function SeedKitCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Spawn a seed kit"
end

function SeedKitCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
