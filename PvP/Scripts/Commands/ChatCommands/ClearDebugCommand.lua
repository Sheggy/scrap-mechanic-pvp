ClearDebugCommand = class(CommandExecutor)
ClearDebugCommand.gameInstance = nil

function ClearDebugCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Clear debug draw objects"
end

function ClearDebugCommand:execute(sender, command)
    sm.debugDraw.clear()

    return true
end
