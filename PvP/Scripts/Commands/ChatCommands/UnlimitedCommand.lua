UnlimitedCommand = class(CommandExecutor)
UnlimitedCommand.gameInstance = nil

function UnlimitedCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Use the unlimited inventory"
end

function UnlimitedCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_setLimitedInventory", false)

    return true
end
