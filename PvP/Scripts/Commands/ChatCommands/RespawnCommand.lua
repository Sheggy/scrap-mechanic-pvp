RespawnCommand = class(CommandExecutor)
RespawnCommand.gameInstance = nil

function RespawnCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Respawn at last bed (or at the crash site)"
end

function RespawnCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_respawnCallback", command:getArguments())

    return true
end
