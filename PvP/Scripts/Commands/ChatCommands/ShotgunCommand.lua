ShotgunCommand = class(CommandExecutor)
ShotgunCommand.gameInstance = nil

function ShotgunCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Give the fries shotgun"
end

function ShotgunCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = tool_shotgun, quantity = 1 })

    return true
end
