GatlingCommand = class(CommandExecutor)
GatlingCommand.gameInstance = nil

function GatlingCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Give the potato gatling gun"
end

function GatlingCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_giveItem", { player = sm.localPlayer.getPlayer(), item = tool_gatling, quantity = 1 })

    return true
end
