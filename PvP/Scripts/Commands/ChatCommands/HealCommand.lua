HealCommand = class(CommandExecutor)
HealCommand.gameInstance = nil

function HealCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Heals health, food and water"
end

function HealCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_healCallback", command:getArguments())

    return true
end
