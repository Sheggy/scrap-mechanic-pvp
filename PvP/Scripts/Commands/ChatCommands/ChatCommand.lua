--[[
    Represents a chat command that was written in the ingame chat
]]
ChatCommand = class(Command)
