--[[
    Handles chat commands and invokes the registered command executor for that command
]]
ChatCommandInvoker = class(CommandInvoker)
