GoToCommand = class(CommandExecutor)
GoToCommand.gameInstance = nil

function GoToCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "string", "name", false }
    }
    self.description = "Teleport to predefined position"
end

function GoToCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_gotoCallback", command:getArguments())

    return true
end
