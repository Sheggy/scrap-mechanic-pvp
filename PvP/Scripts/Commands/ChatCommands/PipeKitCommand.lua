PipeKitCommand = class(CommandExecutor)
PipeKitCommand.gameInstance = nil

function PipeKitCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Spawn a pipe kit"
end

function PipeKitCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
