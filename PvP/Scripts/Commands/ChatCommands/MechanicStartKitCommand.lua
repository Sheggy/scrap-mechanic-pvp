MechanicStartKitCommand = class(CommandExecutor)
MechanicStartKitCommand.gameInstance = nil

function MechanicStartKitCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Spawn a starter kit for starting at mechanic station"
end

function MechanicStartKitCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
