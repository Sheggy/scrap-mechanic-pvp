DisableRaidsCommand = class(CommandExecutor)
DisableRaidsCommand.gameInstance = nil

function DisableRaidsCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "bool", "enabled", false }
    }
    self.description = "Disable raids if true"
end

function DisableRaidsCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
