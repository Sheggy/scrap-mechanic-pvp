ImportCommand = class(CommandExecutor)
ImportCommand.gameInstance = nil

function ImportCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "string", "name", false }
    }
    self.description = "Imports blueprint $SURVIVAL_DATA/LocalBlueprints/<name>.blueprint"
end

function ImportCommand:execute(sender, command)
    local rayCastValid, rayCastResult = sm.localPlayer.getRaycast(100)

    if rayCastValid then
        local importParams = {
            world = sm.localPlayer.getPlayer().character:getWorld(),
            name = command:getArguments()[2],
            position = rayCastResult.pointWorld
        }
        self.gameInstance.network:sendToServer("sv_importCreation", importParams)
    end

    return true
end
