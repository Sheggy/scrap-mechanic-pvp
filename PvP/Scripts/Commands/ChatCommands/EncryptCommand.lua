EncryptCommand = class(CommandExecutor)
EncryptCommand.gameInstance = nil

function EncryptCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Restrict interactions in all warehouses"
end

function EncryptCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_enableRestrictions", true)

    return true
end
