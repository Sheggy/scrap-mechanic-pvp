PrintGlobalsCommand = class(CommandExecutor)
PrintGlobalsCommand.gameInstance = nil

function PrintGlobalsCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Print all global lua variables"
end

function PrintGlobalsCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_printGlobalsCallback", command:getArguments())

    return true
end
