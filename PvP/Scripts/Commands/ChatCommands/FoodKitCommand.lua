FoodKitCommand = class(CommandExecutor)
FoodKitCommand.gameInstance = nil

function FoodKitCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {}
    self.description = "Spawn a food kit"
end

function FoodKitCommand:execute(sender, command)
    self.gameInstance.network:sendToServer("sv_raiseWorldEventCallback", command:getArguments())

    return true
end
