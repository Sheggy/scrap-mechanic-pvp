ExportCommand = class(CommandExecutor)
ExportCommand.gameInstance = nil

function ExportCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "string", "name", false }
    }
    self.description = "Exports blueprint $SURVIVAL_DATA/LocalBlueprints/<name>.blueprint"
end

function ExportCommand:execute(sender, command)
    local rayCastValid, rayCastResult = sm.localPlayer.getRaycast(100)

    if rayCastValid and rayCastResult.type == "body" then
        local importParams = {
            name = command:getArguments()[2],
            body = rayCastResult:getBody()
        }
        self.gameInstance.network:sendToServer("sv_exportCreation", importParams)
    end

    return true
end
