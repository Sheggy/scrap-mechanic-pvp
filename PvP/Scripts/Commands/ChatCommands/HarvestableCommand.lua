HarvestableCommand = class(CommandExecutor)
HarvestableCommand.gameInstance = nil

function HarvestableCommand:__init(gameInstance)
    self.gameInstance = gameInstance
    self.data = {
        { "string", "harvestableName", true }
    }
    self.description = "Create a harvestable: 'tree', 'stone'"
end

function HarvestableCommand:execute(sender, command)
    local character = sm.localPlayer.getPlayer().character

    if character then
        local harvestableUuid = sm.uuid.new("00000000-0000-0000-0000-000000000000")

        if command:getArguments()[2] == "tree" then
            harvestableUuid = sm.uuid.new("c4ea19d3-2469-4059-9f13-3ddb4f7e0b79")
        elseif command:getArguments()[2] == "stone" then
            harvestableUuid = sm.uuid.new("0d3362ae-4cb3-42ae-8a08-d3f9ed79e274")
        elseif command:getArguments()[2] == "soil" then
            harvestableUuid = hvs_soil
        elseif command:getArguments()[2] == "fencelong" then
            harvestableUuid = sm.uuid.new("c0f19413-6d8e-4b20-819a-949553242259")
        elseif command:getArguments()[2] == "fenceshort" then
            harvestableUuid = sm.uuid.new("144b5e79-483e-4da6-86ab-c575d0fdcd11")
        elseif command:getArguments()[2] == "fencecorner" then
            harvestableUuid = sm.uuid.new("ead875db-59d0-45f5-861e-b3075e1f8434")
        elseif command:getArguments()[2] == "beehive" then
            harvestableUuid = hvs_farmables_beehive
        elseif command:getArguments()[2] == "cotton" then
            harvestableUuid = hvs_farmables_cottonplant
        elseif command:getArguments()[2] then
            harvestableUuid = sm.uuid.new(command:getArguments()[2])
        end

        local spawnParams = { world = character:getWorld(), uuid = harvestableUuid, position = character.worldPosition, quat = sm.vec3.getRotation(sm.vec3.new(0, 1, 0), sm.vec3.new(0, 0, 1)) }
        self.gameInstance.network:sendToServer("sv_spawnHarvestable", spawnParams)
    end

    return true
end
