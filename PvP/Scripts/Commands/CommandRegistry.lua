--[[
    Manages commands. Register and deregister commands within the command registry
]]
CommandRegistry = class()
CommandRegistry.commandExecutors = nil

--[[
    Creates a new command registry
]]
function CommandRegistry:__init()
    self.commandExecutors = Dictionary()
end

--[[
    Adds a command executor for a given command text to the registry
    @param command string The command text
    @param executor CommandExecutor The command executor that handles the specified command text
]]
function CommandRegistry:addCommand(command, executor)
    self.commandExecutors:add(command, executor)
    sm.game.bindChatCommand("/" .. command, executor:getData(), "cl_handleCommand", executor:getDescription())
end

--[[
    Removes a command executor with the given command text from the registry
    @param command string The command text
]]
function CommandRegistry:removeCommand(command)
    self.commandExecutors:remove(command)
end

--[[
    Return the command executor for the specified command text if available
    @param command string The command text
    @return bool, CommandExecutor Returns tuple (registered, executor) if found otherwise (false, nil)
]]
function CommandRegistry:getCommandExecutor(command)
    return self.commandExecutors:tryGet(command)
end
