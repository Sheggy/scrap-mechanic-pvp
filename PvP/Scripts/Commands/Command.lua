--[[
    Represents a command
]]
Command = class()
Command.commandText = nil
Command.arguments = nil

--[[
    Creates a new command object
    @param commandText The command text identifying the command
    @param arguments The arguments passed with the command
]]
function Command:__init(commandText, arguments)
    self.commandText = commandText
    self.arguments = arguments
end

--[[
    Returns the command text of the command
    @return string The command text
]]
function Command:getCommandText()
    return self.commandText
end

--[[
    Returns a sorted list of the passed arguments
    @return List A sorted list of the passed arguments
]]
function Command:getArguments()
    return self.arguments
end
