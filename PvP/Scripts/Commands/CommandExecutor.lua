--[[
    Represents a command executor which executes a command
]]
CommandExecutor = class()
CommandExecutor.data = nil
CommandExecutor.description = nil

--[[
    Default constructor
]]
function CommandExecutor:__init()
end

--[[
    Returns the internally used command data
]]
function CommandExecutor:getData()
    return self.data
end

--[[
    Returns the command executors description
]]
function CommandExecutor:getDescription()
    return self.description
end

--[[
    Executes the requested command
    @param sender The orginial sender requesting the command
    @param command The command object
    @return Returns true if the command was successful otherwise false
]]
function CommandExecutor:execute(sender, command)
end
