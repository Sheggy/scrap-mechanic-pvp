dofile(("%s/%s"):format(Game.ScriptPath, "Harvestable.lua"))

OilGeyser = class(nil)

function OilGeyser:client_onInteract(state)
    self.network:sendToServer("sv_n_harvest")
end

function OilGeyser:client_canInteract()
    sm.gui.setInteractionText("", sm.gui.getKeyBinding("Attack"), "#{INTERACTION_PICK_UP}")

    return true
end

function OilGeyser:server_canErase()
    return true
end

function OilGeyser:client_canErase()
    return true
end

function OilGeyser:server_onRemoved(player)
    self:sv_n_harvest(nil, player)
end

function OilGeyser:client_onCreate()
    self.cl = {}
    self.cl.acitveGeyser = sm.effect.createEffect("Oilgeyser - OilgeyserLoop")
    self.cl.acitveGeyser:setPosition(self.harvestable.worldPosition)
    self.cl.acitveGeyser:setRotation(self.harvestable.worldRotation)
    self.cl.acitveGeyser:start()
end

function OilGeyser:cl_n_onInventoryFull()
    sm.gui.displayAlertText("#{INFO_INVENTORY_FULL}", 4)
end

function OilGeyser:sv_n_harvest(params, player)
    if not self.harvested and sm.exists(self.harvestable) then
        local container = player:getInventory()
        local quantity = randomStackAmount(1, 2, 4)

        if sm.container.beginTransaction() then
            sm.container.collect(container, obj_resource_crudeoil, quantity)

            if sm.container.endTransaction() then
                sm.event.sendToPlayer(player, "sv_e_onLoot", { uuid = obj_resource_crudeoil, quantity = quantity, pos = self.harvestable.worldPosition })
                sm.effect.playEffect("Oilgeyser - Picked", self.harvestable.worldPosition)
                sm.harvestable.create(hvs_farmables_growing_oilgeyser, self.harvestable.worldPosition, self.harvestable.worldRotation)
                sm.harvestable.destroy(self.harvestable)
                self.harvested = true
            else
                self.network:sendToClient(player, "cl_n_onInventoryFull")
            end
        end
    end
end

function OilGeyser:client_onDestroy()
    self.cl.acitveGeyser:stop()
    self.cl.acitveGeyser:destroy()
end
