dofile(("%s/%s"):format(Game.ScriptPath, "Loot.lua"))

GrowingCottonPlant = class()

local GrowTickTime = DAYCYCLE_TIME_TICKS * 2.5

-- Server
function GrowingCottonPlant:server_onCreate()
    self.sv = self.storage:load()

    if self.sv == nil then
        self.sv = {}
        self.sv.lastTickUpdate = sm.game.getCurrentTick()
        self.sv.growTicks = 0
    end
end

function GrowingCottonPlant:server_onReceiveUpdate()
    self:sv_performUpdate()
end

function GrowingCottonPlant:sv_performUpdate()
    local currentTick = sm.game.getCurrentTick()
    local ticks = currentTick - self.sv.lastTickUpdate
    ticks = math.max(ticks, 0)
    self.sv.lastTickUpdate = currentTick
    self:sv_updateTicks(ticks)
    self.storage:save(self.sv)
end

function GrowingCottonPlant:sv_updateTicks(ticks)
    if not self.sv.regrown and sm.exists(self.harvestable) then
        self.sv.growTicks = math.min(self.sv.growTicks + ticks, GrowTickTime)
        local growFraction = self.sv.growTicks / GrowTickTime

        if growFraction >= 1.0 then
            sm.harvestable.create(hvs_farmables_cottonplant, self.harvestable.worldPosition, self.harvestable.worldRotation)
            sm.harvestable.destroy(self.harvestable)
            self.sv.regrown = true
        end
    end
end
