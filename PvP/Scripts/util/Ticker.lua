Ticker = class(nil)

function Ticker:init()
    self.tickers = {}
end

function Ticker:addState(state)
    if state.tick then
        self.tickers[#self.tickers + 1] = state
    end
end

function Ticker:tick()
    for _, t in ipairs(self.tickers) do
        t:tick()
    end
end
