Stack = class(nil)

function Stack:init()
    self.stack = {}
end

function Stack:push(item)
    self.stack[#self.stack + 1] = item
end

function Stack:pop()
    return table.remove(self.stack, #self.stack)
end
