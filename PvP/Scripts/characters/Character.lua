Character = class(nil)

--[[ Callbacks ]]

function Character:client_onCreate()
end

-- Callback when character is destroyed
function Character:client_onDestroy()
end

-- Callback when script is updated
function Character:client_onRefresh()
end

-- Callback when character is updated
function Character:client_onUpdate(deltaTime)
end

function Character:client_onEvent(event)
end
