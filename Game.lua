Game = {}
Game.config = sm.json.open("$SURVIVAL_DATA/Game/config.json")
Game = Game.config.GameModeList[Game.config.GameMode]

dofile(("%s/%s"):format(Game.ScriptPath, Game.StartScript))
dofile(("%s/%s"):format(Game.ScriptPath, Game.PlayerScript))
dofile(("%s/%s"):format(Game.ScriptPath, Game.CharacterScript))
dofile(("%s/%s"):format(Game.ScriptPath, Game.ContainerScript))
dofile(("%s/%s"):format(Game.ScriptPath, Game.HarvestableScript))
dofile(("%s/%s"):format(Game.ScriptPath, Game.InteractableScript))
dofile(("%s/%s"):format(Game.ScriptPath, Game.ToolScript))
dofile(("%s/%s"):format(Game.ScriptPath, Game.UnitScript))
